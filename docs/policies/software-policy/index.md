# NERSC Software Support Policy

The intent of the NERSC Software Support Policy is to provide the formal means
to balance user software environment stability, facilitate user access to new
and upgraded software, better allocate NERSC staff support effort and user
development effort, enable system administration flexibility, and support
institutional strategic goals.

## Policy Summary

* The NERSC Software Policy Committee will assign one of four
  [support levels](https://gitlab.com/NERSC/software-policy/blob/master/3-levels.md)
  to all user software on our production systems.
  
    | **Support Level Name**       | Restricted | Minimal | Provided | Priority | 
    |------------------------------|:----------:|:-------:|:--------:|:--------:|
    | **Allowed at NERSC?**        | No         | Yes     | Yes      | Yes      |
    | **Provided by NERSC?**       | No         | No[^1]  | Yes      | Yes      |
    | **NERSC support priority?**  | No         | Low     | Moderate | High     |
    | **Tests validate function?** | No         | No      | Yes      | Yes      |
    | **Performance tests?**       | No         | No      | No       | Yes      |

[^1]: NERSC doesn't commit to providing Minimal-support software, but might
      make a "no guarantees" installation of a package accessible to users
      nonetheless

* Support levels will not be reduced without at least six months advance
  notice, except in cases where a software package is made **Restricted**

* The software
  [building and installation practices we use will be standardized](https://gitlab.com/NERSC/software-policy/blob/master/2-build.md)
  and [made more available to users](build_resources.md).

The policy can be viewed in full detail
[here](https://gitlab.com/NERSC/software-policy).

## Current State of Policy Implementation

During 2020, NERSC will transition into using this formal policy to guide
software support decisions. NERSC support in practice will not change until
the Software Policy Committee has completed the process of assigning support
levels to all software.
  
To bridge this transition period, user software installed by NERSC and present
our production systems at the beginning of AY 2020, but not yet assigned a
support level, will be supported as if it is "Provided." User software that has
not been installed by NERSC will be supported as if it were "Minimal."

## Additional Information 

[**FAQ:** Frequently asked questions about this software policy.](faq.md)

* [Where to find information about support levels and status of specific software.](software_state.md)
* [Where to find resources used to build supported software.](build_resources.md)
