# Spin Migration Guide (Rancher 2)

_This guide is under development with information specific to Spin._

Please consult the [migration guide on the Rancher web
site](https://rancher.com/docs/rancher/v2.x/en/v1.6-migration/)
for a general overview of the migration process along with
several examples.
